/*
 * include/linux/sis_i2c.h - platform data structure for SiS 9200 family
 *
 * Copyright (C) 2011 SiS, Inc.
 * Copyright (C) 2015 Nextfour Group
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
#include <linux/version.h>

#ifndef _LINUX_SIS_I2C_H
#define _LINUX_SIS_I2C_H

#define SIS_I2C_NAME "sis9252"
#define SIS_SLAVE_ADDR					0x5c

#define MAX_FINGERS					10

#define SIS_MAX_X					4095
#define SIS_MAX_Y					4095

#define ONE_BYTE					1
#define FIVE_BYTE					5
#define EIGHT_BYTE					8
#define SIXTEEN_BYTE					16
#define PACKET_BUFFER_SIZE				128

#define SIS_CMD_NORMAL					0x0
#define SIS_CMD_SOFTRESET				0x82
#define SIS_CMD_RECALIBRATE				0x87
#define SIS_CMD_POWERMODE				0x90
#define MSK_TOUCHNUM					0x0f
#define MSK_HAS_CRC					0x10
#define MSK_DATAFMT					0xe0
#define MSK_PSTATE				        0x0f
#define MSK_PID                                         0xf0
#define RES_FMT						0x00
#define FIX_FMT						0x40

/* for new i2c format */
#define TOUCHDOWN                                       0x3
#define TOUCHUP						0x0
#define MAX_BYTE					64
#define	PRESSURE_MAX                                    255

/* Resolution diagonal */
#define AREA_LENGTH_LONGER				5792
/*((SIS_MAX_X^2) + (SIS_MAX_Y^2))^0.5*/
#define AREA_LENGTH_SHORT				5792
#define AREA_UNIT					(5792/32)


#define FORMAT_MODE					1

#define MSK_NOBTN					0
#define MSK_COMP					1
#define MSK_BACK					2
#define MSK_MENU					4
#define MSK_HOME					8

#define P_BYTECOUNT					0
#define ALL_IN_ONE_PACKAGE				0x10
#define IS_TOUCH(x)					(x & 0x1)
#define IS_HIDI2C(x)					((x & 0xF) == 0x06)
#define IS_AREA(x)					((x >> 4) & 0x1)
#define IS_PRESSURE(x)				        ((x >> 5) & 0x1)
#define IS_SCANTIME(x)			                ((x >> 6) & 0x1)

#define NORMAL_LEN_PER_POINT			        6
#define AREA_LEN_PER_POINT				2
#define PRESSURE_LEN_PER_POINT			        1

#define TOUCH_FORMAT					0x1
#define BUTTON_FORMAT					0x4
#define HIDI2C_FORMAT					0x6
#define P_REPORT_ID					2
#define BUTTON_STATE					3
#define BUTTON_KEY_COUNT				16
#define BYTE_BYTECOUNT					2
#define BYTE_COUNT					1
#define BYTE_REPORTID					1
#define BYTE_CRC_HIDI2C					0
#define BYTE_CRC_I2C					2
#define BYTE_SCANTIME					2
#define NO_TOUCH_BYTECOUNT				0x3

/* CMD Define */
#define BUF_ACK_PLACE_L					4
#define BUF_ACK_PLACE_H					5
#define BUF_ACK_L					0xEF
#define BUF_ACK_H					0xBE
#define BUF_NACK_L					0xAD
#define BUF_NACK_H					0xDE
#define BUF_CRC_PLACE					7

/* SiS i2c error code */
#define SIS_ERR						-1
#define SIS_ERR_ACCESS_USER_MEM		-11 /* Access user memory fail */
#define SIS_ERR_ALLOCATE_KERNEL_MEM	-12 /* Allocate memory fail */
#define SIS_ERR_CLIENT			-13 /* Client not created */
#define SIS_ERR_COPY_FROM_USER		-14 /* Copy data from user fail */
#define SIS_ERR_COPY_FROM_KERNEL	-19 /* Copy data from kernel fail */
#define SIS_ERR_TRANSMIT_I2C		-21 /* Transmit error in I2C */

struct sis_i2c_rmi_platform_data {
	int (*power)(int on);	/* Only valid in first array entry */
};

struct _touchpoint {
	int id;
	unsigned short x, y;
	uint16_t pressure;
	uint16_t width;
	uint16_t height;
};

struct sistp_driver_data {
	int id;
	int fingers;
	uint8_t pre_keybit_state;
	struct _touchpoint pt[MAX_FINGERS];
};

struct sis_ts_data {
	int (*power)(int on);
	int irq_gpio;
	int reset_gpio;
	struct i2c_client *client;
	struct input_dev *input_dev;
	struct sistp_driver_data tpinfo;
};

#endif /* _LINUX_SIS_I2C_H */
